#![allow(dead_code)]

pub const S_IFMT: u16 = 0o0170000;

pub fn fmt(f: u16) -> u16 {
    f & S_IFMT
}

pub fn is_reg(f: u16) -> bool {
    fmt(f) == S_IFREG
}

pub fn is_dir(f: u16) -> bool {
    fmt(f) == S_IFDIR
}

pub const S_IFSOCK: u16 = 0o140000;
pub const S_IFLNK: u16 = 0o120000;
pub const S_IFREG: u16 = 0o100000;
pub const S_IFBLK: u16 = 0o060000;
pub const S_IFDIR: u16 = 0o040000;
pub const S_IFCHR: u16 = 0o020000;
pub const S_IFIFO: u16 = 0o010000;
pub const S_ISUID: u16 = 0o004000;
pub const S_ISGID: u16 = 0o002000;
pub const S_ISVTX: u16 = 0o001000;

pub const S_IRWXU: u16 = 0o0700;
pub const S_IRUSR: u16 = 0o0400;
pub const S_IWUSR: u16 = 0o0200;
pub const S_IXUSR: u16 = 0o0100;

pub const S_IRWXG: u16 = 0o0070;
pub const S_IRGRP: u16 = 0o0040;
pub const S_IWGRP: u16 = 0o0020;
pub const S_IXGRP: u16 = 0o0010;

pub const S_IRWXO: u16 = 0o0007;
pub const S_IROTH: u16 = 0o0004;
pub const S_IWOTH: u16 = 0o0002;
pub const S_IXOTH: u16 = 0o0001;

#[derive(Debug)]
pub struct Stat {
    pub inodenum: u32,
    pub mode: u16,
    pub nlinks: u16,
    pub uid: u16,
    pub gid: u16,
    pub size: u64,
    pub atime: u32,
    pub mtime: u32,
    pub ctime: u32,
}
