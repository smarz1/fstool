use crate::mach::Machine;
use std::io::stdin;

type CommandFunc = fn(&mut Machine, Vec<&str>);
pub struct Command<'a> {
    pub cmd: Vec<&'a str>,
    pub func: CommandFunc,
    pub help: &'a str,
}
impl<'a> Command<'a> {
    pub fn new_with(cmd: Vec<&'a str>, func: CommandFunc, help: &'a str) -> Self {
        Self { cmd, func, help }
    }

    pub fn has_cmd(&self, cmd: &str) -> bool {
        matches!(self.cmd.binary_search(&cmd), Ok(_))
    }
}

fn find_cmd<'a>(cmds: &'a Vec<Command<'a>>, cmd: &str) -> Option<CommandFunc> {
    for i in cmds {
        if i.has_cmd(cmd) {
            return Some(i.func);
        }
    }
    None
}

fn print_help<'a>(cmds: &'a [Command<'a>]) {
    for i in cmds {
        let mut m = String::new();
        for j in 0..i.cmd.len() {
            m += i.cmd[j];
            m += ", ";
        }
        let _ = m.pop();
        let _ = m.pop();
        println!("{:<20} - {}", m, i.help);
    }
}

pub fn console(mach: &mut Machine, cmds: &Vec<Command>) {
    let stdin = stdin();
    let mut line = String::new();

    eprintln!("Type 'help' for help.");
    while mach.running {
        eprint!("{}> ", mach.cwd);
        if stdin.read_line(&mut line).is_ok() {
            if line.is_empty() {
                println!();
                break;
            }
            let _ = line.pop();
            let mut data = line.split(' ');
            let c = data.next().unwrap();
            if c == "help" {
                // Handle "help" differently and take it away.
                print_help(cmds);
            }
            else if let Some(runner) = find_cmd(cmds, c) {
                // We found the command in the vector
                runner(mach, data.collect());
            }
            else if !c.is_empty() {
                // No commands matched
                println!("Unknown command '{}'", line);
            }
            line.clear();
        }
        else {
            break;
        }
    }
}
