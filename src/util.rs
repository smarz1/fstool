//! ## util.rs
//! This library contains several helpful utilities to read and write
//! as well as cast data from one way to another. It contains many
//! unsafe blocks, so work will be done to ensure the underlying unsafe
//! data is verified before it leaves the unsafe blocks.
#![allow(dead_code)]
use std::boxed::Box;
use std::fs::File;
use std::io::{BufReader, Error, ErrorKind, Read, Seek, SeekFrom};
use std::mem::size_of;

/// # Overview
/// Convert a group of bytes into a reference for data type T.
/// # Examples
/// ```rust
/// let myint = conv_ref::<i32>(&myu8vec, 16).unwrap();
/// ```
/// The line above will convert a reference of u8s (bytes) starting
/// at offset 16. So, bytes 16, 17, 18, and 19 (i32 = 4 bytes) will
/// be referenced.
/// # Returns
/// A Result of a read-only reference to the desired data type or an Error
pub fn conv_ref<T>(data: &[u8], from: usize) -> Result<&T, Error> {
    let to = from + size_of::<T>();
    if data.len() - from < size_of::<T>() {
        Err(Error::new(
            ErrorKind::InvalidData,
            "data.len() >= size_of::<T>()",
        ))
    }
    else if data.len() < to {
        Err(Error::new(
            ErrorKind::InvalidData,
            "data.len() + from >= size_of::<T>()",
        ))
    }
    else {
        let slc = &data[from..to];
        Ok(unsafe { (slc as *const _ as *mut T).as_ref().unwrap() })
    }
}

/// # Overview
/// Convert a group of bytes into a reference for data type T.
/// # Examples
/// ```rust
/// let myint = conv_mut::<i32>(&myu8vec, 16).unwrap();
/// ```
/// The line above will convert a reference of u8s (bytes) starting
/// at offset 16. So, bytes 16, 17, 18, and 19 (i32 = 4 bytes) will
/// be referenced.
/// # Returns
/// A Result of a mutable reference to the desired data type or an Error
pub fn conv_mut<T: Sized>(data: &mut [u8], from: usize) -> Result<&mut T, Error> {
    let to = from + size_of::<T>();
    if data.len() - from < size_of::<T>() {
        Err(Error::new(
            ErrorKind::InvalidData,
            "data.len() >= size_of::<T>()",
        ))
    }
    else if data.len() < to {
        Err(Error::new(
            ErrorKind::InvalidData,
            "data.len() + from >= size_of::<T>()",
        ))
    }
    else {
        let slc = &data[from..to];
        Ok(unsafe { (slc as *const _ as *mut T).as_mut().unwrap() })
    }
}

pub fn conv_bytes<T: Sized>(data: &[u8]) -> Result<Box<T>, Error> {
    if data.len() < size_of::<T>() {
        Err(Error::new(
            ErrorKind::InvalidData,
            "data.len() >= size_of::<T>()",
        ))
    }
    else {
        Ok(unsafe { Box::from_raw(data as *const _ as *mut T) })
    }
}
pub fn read_bytes<T: Sized>(fin: &mut BufReader<File>, offset: usize) -> Result<Box<T>, Error>
{
    fin.seek(SeekFrom::Start(offset as u64))?;
    let mut buffer = vec![0; size_of::<T>()];
    let n = fin.read(&mut buffer)?;
    if n != buffer.len() {
        Err(Error::new(ErrorKind::InvalidData, "n != buffer.len()"))
    }
    else {
        conv_bytes(buffer.leak())
    }
}
pub fn bytes_to_string(data: &[u8]) -> String {
    let mut ret = String::with_capacity(data.len());
    for i in data.iter().copied() {
        if i == 0 {
            break;
        }
        ret.push(i as char);
    }
    ret
}
pub fn bitmap_to_string(data: &[u8]) -> String {
    let mut ret = String::with_capacity(data.len() * 10);
    let mut z = 0;
    for d in data.iter().copied() {
        for j in 0..8 {
            let b = 0 != (d & (1 << j));
            if b {
                ret.push('1');
            }
            else {
                ret.push('0');
            }
        }
        if z >= 7 {
            ret.push('\n');
            z = 0;
        }
        else {
            ret.push(' ');
            z += 1;
        }
    }
    ret
}

pub fn count_slashes(path: &str) -> usize {
    let mut num_slashes = 0;
    for c in path.chars() {
        if c == '/' {
            num_slashes += 1;
        }
    }
    num_slashes
}

pub fn split_path(path: &str) -> Vec<&str> {
    let mut p: Vec<&str> = path.split('/').collect();
    if path.starts_with('/') {
        p.remove(0);
    }
    p
}

pub fn join_path(path: &Vec<&str>, abs: bool) -> String {
    let mut ret = if abs {
        String::from('/')
    }
    else {
        String::new()
    };
    for p in path {
        ret += *p;
        ret += "/";
    }
    ret.pop();
    ret
}
