//! ## minix3.rs
//! Minix 3 file system implementation.
#![allow(dead_code)]

use crate::stat::{is_dir, is_reg, Stat};
use crate::util::{bytes_to_string, conv_mut, conv_ref, read_bytes, split_path};
use crate::vfs::{FsDriver, Node};
use std::{fs::File, io::BufReader, io::BufWriter, io::Write, mem::size_of};

const NAME_LEN: usize = 60;
const BOOT_BLOCK_SIZE: usize = 1024;
const SUPER_BLOCK_SIZE: usize = 1024;
const MAGIC: u16 = 0x4d5a;
const BITMAP_ALLOCATED: u8 = 1;
const BITMAP_UNALLOCATED: u8 = 0;
const DIRENTRY_SIZE: usize = size_of::<DirEntry>();
const NUM_ZONES: usize = 10;

#[repr(C)]
pub struct SuperBlock {
    pub inodes: u32,
    pub pad0: u16,
    pub imap_blocks: u16,
    pub zmap_blocks: u16,
    pub first_data_zone: u16,
    pub log_zone_size: u16,
    pub pad1: u16,
    pub max_size: u32,
    pub zones: u32,
    pub magic: u16,
    pub pad2: u16,
    pub pad3: u16,
    pub disk_version: u8,
}

impl SuperBlock {
    pub fn find_inode(&self, num: u32) -> usize {
        let n = num as usize - 1;
        let bs = self.zone_size();
        let ib = self.imap_blocks as usize;
        let zb = self.zmap_blocks as usize;
        BOOT_BLOCK_SIZE + SUPER_BLOCK_SIZE + (n * size_of::<Inode>()) + (ib * bs) + (zb * bs)
    }

    pub fn zone_size(&self) -> usize {
        1024 << self.log_zone_size
    }
}

#[repr(C)]
pub struct Inode {
    pub mode: u16,
    pub nlinks: u16,
    pub uid: u16,
    pub gid: u16,
    pub size: u32,
    pub atime: u32,
    pub mtime: u32,
    pub ctime: u32,
    pub zones: [u32; NUM_ZONES],
}

#[repr(C)]
pub struct DirEntry {
    pub inode: u32,
    pub name: [u8; NAME_LEN],
}

pub struct Minix3;

/// # Identify a Minix 3 file system.
/// ## Arguments
/// - `reader` - A Mutable reference to a BufReader<File> that contains the file system.
/// ## Returns
/// - `true` - the file system is a Minix 3 file system.
/// - `false` - the file system is not a Minix 3 file system.
pub fn id(reader: &mut BufReader<File>) -> bool {
    matches!(read_bytes::<SuperBlock>(reader, BOOT_BLOCK_SIZE), Ok(sb) if sb.magic == MAGIC)
}

impl FsDriver for Minix3 {
    fn name(&self) -> &str {
        "minix3"
    }

    fn cache(&mut self, bdev: &[u8]) -> Vec<Node> {
        let mut nodes = vec![];
        cache_dir(
            bdev,
            1,
            &mut nodes,
            conv_ref(bdev, BOOT_BLOCK_SIZE).unwrap(),
        );
        nodes
    }

    fn create(&mut self, data: &mut [u8], path: &str, mode: u16) -> usize {
        let _p = split_path(path);
        if let Some(inode) = take_next_inode(data) {
            let inum = conv_ref::<SuperBlock>(data, BOOT_BLOCK_SIZE)
                .unwrap()
                .find_inode(inode);
            let ino = conv_mut::<Inode>(data, inum).unwrap();
            ino.mode = mode;
            ino.nlinks = 1;
            ino.size = 0;
            ino.uid = 1000;
            ino.gid = 1000;
            ino.zones[0] = 0;
            inode as usize
        }
        else {
            0
        }
    }

    fn flush(&self, bdev: &[u8], out: &mut BufWriter<File>) -> bool {
        matches!(out.write_all(bdev), Ok(_))
    }
}

fn zone_taken(data: &[u8], zone: u32) -> bool {
    let sb = conv_ref::<SuperBlock>(data, BOOT_BLOCK_SIZE).unwrap();
    let zone_byte = zone / 8;
    let zone_bit = zone % 8;
    let inode_zone_skip = sb.imap_blocks as usize * sb.zone_size();
    let offset = BOOT_BLOCK_SIZE * 2 + inode_zone_skip + zone_byte as usize;
    let bm = conv_ref::<u8>(data, offset).unwrap();

    bm >> zone_bit & 1 == 1
}

fn take_next_inode(data: &mut [u8]) -> Option<u32> {
    let sb = conv_ref::<SuperBlock>(data, BOOT_BLOCK_SIZE).unwrap();
    for i in 1..sb.inodes {
        if !inode_taken(data, i) {
            let inode_byte = i / 8;
            let inode_bit = i % 8;
            let offset = BOOT_BLOCK_SIZE * 2 + inode_byte as usize;
            let bm = conv_mut::<u8>(data, offset).unwrap();
            *bm |= 1 << inode_bit;
            return Some(i);
        }
    }
    None
}

fn inode_taken(data: &[u8], inode: u32) -> bool {
    let inode_byte = inode / 8;
    let inode_bit = inode % 8;
    let offset = BOOT_BLOCK_SIZE * 2 + inode_byte as usize;
    let bm = conv_ref::<u8>(data, offset).unwrap();

    bm >> inode_bit & 1 == 1
}

fn cache_dir(data: &[u8], inode: u32, nodes: &mut Vec<Node>, sb: &SuperBlock) {
    let mut children: Vec<Node> = vec![];
    let ino = conv_ref::<Inode>(data, sb.find_inode(inode)).unwrap();
    let mut ptrs = vec![];

    // Direct zones
    for i in 0..7 {
        let ptr = ino.zones[i];
        if ptr != 0 {
            ptrs.push(ptr);
        }
    }

    let mut ind = get_ind(data, ino.zones[7], sb);
    ptrs.append(&mut ind);
    let mut dind = get_dind(data, ino.zones[8], sb);
    ptrs.append(&mut dind);
    let mut tind = get_tind(data, ino.zones[9], sb);
    ptrs.append(&mut tind);

    // If all the functions worked properly, these pointers now point to actual DirEntry
    // blocks.

    for i in ptrs {
        let offset = i as usize * sb.zone_size();
        let num_dirs = sb.zone_size() / size_of::<DirEntry>();
        for j in 0..num_dirs {
            let doffset = j * size_of::<DirEntry>() + offset;
            let dentry = conv_ref::<DirEntry>(data, doffset).unwrap();
            if dentry.inode != 0 {
                let ino = conv_ref::<Inode>(data, sb.find_inode(dentry.inode)).unwrap();
                if is_dir(ino.mode) {
                    let mut subnodes = vec![];
                    if j > 1 {
                        cache_dir(data, dentry.inode, &mut subnodes, sb);
                    }
                    let child = Node::Directory(
                        bytes_to_string(&dentry.name),
                        dentry.inode as usize,
                        inode_to_stat(dentry.inode, ino),
                        subnodes,
                    );
                    children.push(child);
                }
                else if is_reg(ino.mode) {
                    let child = Node::File(
                        bytes_to_string(&dentry.name),
                        dentry.inode as usize,
                        inode_to_stat(dentry.inode, ino),
                    );
                    children.push(child);
                }
            }
        }
    }
    nodes.append(&mut children);
}

fn get_ind(data: &[u8], ind_ptr: u32, sb: &SuperBlock) -> Vec<u32> {
    if ind_ptr == 0 {
        return vec![];
    }
    let zsize = sb.zone_size() as u32;
    let mut ret = vec![];
    let loc = ind_ptr * zsize;

    for i in (0..zsize).step_by(4) {
        let from = (i + loc) as usize;
        let ptr = conv_ref::<u32>(data, from).unwrap();
        let pval = *ptr;
        if pval != 0 {
            ret.push(pval);
        }
    }

    ret
}

fn get_dind(data: &[u8], dind_ptr: u32, sb: &SuperBlock) -> Vec<u32> {
    let mut ret = vec![];
    if dind_ptr == 0 {
        return ret;
    }
    let ivec = get_ind(data, dind_ptr, sb);
    for i in ivec {
        let mut ptrs = get_ind(data, i, sb);
        ret.append(&mut ptrs);
    }

    ret
}

fn get_tind(data: &[u8], tind_ptr: u32, sb: &SuperBlock) -> Vec<u32> {
    let mut ret = vec![];
    if tind_ptr != 0 {
        let dvec = get_dind(data, tind_ptr, sb);
        for i in dvec {
            let mut ptrs = get_ind(data, i, sb);
            ret.append(&mut ptrs);
        }
    }
    ret
}

fn inode_to_stat(inodenum: u32, inode: &Inode) -> Stat {
    Stat {
        inodenum,
        mode: inode.mode,
        nlinks: inode.nlinks,
        uid: inode.uid,
        gid: inode.gid,
        size: inode.size as u64,
        atime: inode.atime,
        mtime: inode.mtime,
        ctime: inode.ctime,
    }
}
