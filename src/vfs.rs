use crate::stat::Stat;
use std::fs::File;
use std::io::BufWriter;

pub enum Node {
    File(String, usize, Stat),                 // direntry, inode, stat
    Directory(String, usize, Stat, Vec<Node>), // direntry, inode, stat, children
}

pub trait FsDriver {
    fn name(&self) -> &str {
        ""
    }
    fn cache(&mut self, _bdev: &[u8]) -> Vec<Node> {
        vec![]
    }
    fn stat(&mut self, _bdev: &[u8], _inode: usize) -> Option<Stat> {
        None
    }
    fn create(&mut self, _bdev: &mut [u8], _path: &str, _mode: u16) -> usize {
        0
    }
    fn read(&mut self, _bdev: &[u8], _inode: usize, _data: &mut [u8]) -> Option<usize> {
        None
    }
    fn write(&mut self, _bdev: &mut [u8], _inode: usize, _data: &[u8]) -> Option<usize> {
        None
    }
    fn truncate(&mut self, _bdev: &mut [u8], _inode: usize, _size: usize) {}
    fn flush(&self, _bdev: &[u8], _out: &mut BufWriter<File>) -> bool {
        false
    }
}
