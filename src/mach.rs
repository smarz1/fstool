//! ## mach.rs
//! State machine class and utilities.
use crate::vfs::{FsDriver, Node};
use std::collections::BTreeMap;
use std::fs::File;
use std::io::{BufReader, Read, Seek, SeekFrom};

pub struct Machine<'a> {
    pub dirtree: BTreeMap<u32, u32>,
    pub running: bool,
    pub cwd: String,
    pub driver: &'a mut dyn FsDriver,
    pub cache: Vec<Node>,
    pub data: Vec<u8>,
    pub dirty: bool,
}

impl<'a> Machine<'a> {
    pub fn new(reader: &mut BufReader<File>, driver: &'a mut dyn FsDriver) -> Option<Self> {
        let data = get_data(reader);
        if data.is_empty() {
            return None;
        }
        let cache = driver.cache(&data);
        let ret = Self {
            dirtree: BTreeMap::<u32, u32>::new(),
            running: true,
            cwd: String::from("/"),
            driver,
            cache,
            data,
            dirty: false,
        };
        Some(ret)
    }
}

fn get_data(r: &mut BufReader<File>) -> Vec<u8> {
    let _ = r.seek(SeekFrom::End(0));
    let num_bytes = r.stream_position().unwrap() as usize;
    // Make sure we don't use too much memory
    if num_bytes > 1 << 25 {
        return vec![];
    }
    let _ = r.rewind();
    let mut buffer: Vec<u8> = vec![0; num_bytes];
    let n = r.read(&mut buffer).unwrap();
    if n != buffer.len() {
        vec![]
    } else {
        buffer
    }
}
