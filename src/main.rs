use crate::vfs::FsDriver;
use cmd::Command;
use mach::Machine;
use std::env::args;
use std::fs::File;
use std::io::{BufReader, BufWriter, Read};

fn main() {
    let a: Vec<String> = args().collect();
    if a.len() < 2 {
        println!("Usage: {} <block file>", a[0]);
        return;
    }

    let f = File::open(&a[1]);
    let mut fin = BufReader::new(f.ok().unwrap());
    let mut e4drv = ext4::Ext4;
    let mut m3drv = minix3::Minix3;
    let driver = if ext4::id(&mut fin) {
        &mut e4drv as &mut dyn FsDriver
    }
    else if minix3::id(&mut fin) {
        &mut m3drv as &mut dyn FsDriver
    }
    else {
        println!("Unknown file system.");
        return;
    };
    if let Some(mut m) = Machine::new(&mut fin, driver) {
        let cmds = vec![
            Command::new_with(vec!["q", "qu", "qui", "quit"], c_quit, "Quit the program."),
            Command::new_with(vec!["cd"], c_cd, "Change directories."),
            Command::new_with(vec!["l", "ls"], c_ls, "List files and directories."),
            Command::new_with(
                vec!["ll", "lls"],
                c_lls,
                "List files and directories on local storage.",
            ),
            Command::new_with(vec!["cat", "type"], c_cat, "Print out a text file."),
            Command::new_with(
                vec!["fstype", "id"],
                c_id,
                "Identify the file system type detected.",
            ),
            Command::new_with(vec!["rem", "rm"], c_rm, "Remove a file."),
            Command::new_with(vec!["rmdir"], c_rmdir, "Remove a directory."),
            Command::new_with(vec!["mkdir"], c_mkdir, "Make a directory."),
            Command::new_with(vec!["move", "mv"], c_mv, "Move a directory or file."),
            Command::new_with(vec!["copy", "cp"], c_cp, "Copy from one path to another."),
            Command::new_with(
                vec!["chmod"],
                c_chmod,
                "Change permissions of a file or directory.",
            ),
            Command::new_with(
                vec!["g", "ge", "get"],
                c_get,
                "Transfer from filesystem to local filesystem.",
            ),
            Command::new_with(
                vec!["p", "pu", "put"],
                c_put,
                "Transfer from local filesystem to filesystem.",
            ),
            Command::new_with(
                vec!["save"],
                c_save,
                "Save changes to the file system to the disk.",
            ),
        ];
        cmd::console(&mut m, &cmds);
    }
    else {
        println!("Unable to open and read block device.");
    }
}

mod cmd;
mod ext4;
mod fat32;
mod mach;
mod minix3;
mod stat;
mod util;
mod vfs;

fn c_lls(_mach: &mut Machine, _args: Vec<&str>) {
    println!("lls");
}

fn c_cp(_mach: &mut Machine, _args: Vec<&str>) {
    println!("cp");
}

fn c_cat(_mach: &mut Machine, _args: Vec<&str>) {
    println!("cat");
}

fn c_rm(_mach: &mut Machine, _args: Vec<&str>) {
    println!("rm");
}

fn c_rmdir(_mach: &mut Machine, _args: Vec<&str>) {
    println!("rmdir");
}

fn c_mkdir(mach: &mut Machine, _args: Vec<&str>) {
    let m = mach.driver.create(&mut mach.data, "/john", 0o755);
    mach.dirty = true;
    println!("mkdir {}", m);
}

fn c_mv(mach: &mut Machine, _args: Vec<&str>) {
    println!("mv");
    mach.dirty = true;
}

fn c_id(mach: &mut Machine, _args: Vec<&str>) {
    println!("{}", mach.driver.name());
}

fn c_chmod(mach: &mut Machine, _args: Vec<&str>) {
    mach.dirty = true;
}

fn c_cd(mach: &mut Machine, args: Vec<&str>) {
    if args.is_empty() {
        mach.cwd = String::from("/")
    }
    else {
        mach.cwd += args[0];
    }
}

fn c_put(mach: &mut Machine, args: Vec<&str>) {
    if args.is_empty() {
        println!("Usage: put <file> [new file name]");
    }
    else if let Ok(f) = File::open(args[0]) {
        let mut fin = BufReader::new(f);
        let mut data = vec![];
        if let Ok(bytes) = fin.read_to_end(&mut data) {
            mach.driver.write(&mut mach.data, 0, &data);
            println!("Wrote {} bytes.", bytes);
            mach.dirty = true;
        }
        else {
            println!("Unable to read '{}'.", args[0]);
        }
    }
    else {
        println!("Unable to open '{}'.", args[0]);
    }
}

fn c_get(_mach: &mut Machine, args: Vec<&str>) {
    println!("Get {:?}", args);
}

fn c_save(mach: &mut Machine, args: Vec<&str>) {
    if args.is_empty() {
        println!("Usage: save <output>");
        return;
    }
    if let Ok(fl) = File::create(args[0]) {
        let mut out = BufWriter::new(fl);
        mach.driver.flush(&mach.data, &mut out);
        mach.dirty = false;
        println!("File '{}' written.", args[0]);
    }
    else {
        println!("Error writing to '{}'", args[0]);
    }
}

const COLOR_BLUE: &str = "\x1b[38;2;0;0;255m";
// const COLOR_WHITE: &str = "\x1b[38;2;255;255;255m";
const COLOR_GREEN: &str = "\x1b[38;2;0;255;0m";
const COLOR_NORM: &str = "\x1b[38;0;0m";

fn c_ls(mach: &mut Machine, _args: Vec<&str>) {
    let mut num_printed = 0;
    for i in 0..mach.cache.len() {
        match &mach.cache[i] {
            vfs::Node::Directory(name, _inode, _stat, _children) => {
                print!("{}{}{}/    ", COLOR_BLUE, name, COLOR_NORM);
                num_printed += 1;
            }
            vfs::Node::File(name, _inode, stat) => {
                if stat.mode & 0o111 != 0 {
                    print!("{}", COLOR_GREEN);
                }
                print!("{}{}    ", name, COLOR_NORM);
                num_printed += 1;
            }
        }
        if num_printed >= 6 {
            println!();
            num_printed = 0;
        }
    }
    if num_printed != 0 {
        println!();
    }
}

fn c_quit(mach: &mut Machine, args: Vec<&str>) {
    let rr = !args.is_empty() && args[0] == "!";
    if mach.dirty && !rr {
        println!("File system has not been saved. Type quit ! to quit without saving.");
        return;
    }
    mach.running = false;
}
