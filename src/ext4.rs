//! ## ext4.rs
//! Extended file system version 4 utilities.
#![allow(dead_code)]
use std::fmt::{Debug, Error, Formatter};

pub const EXT4_NDIR_BLOCKS: usize = 12;
pub const EXT4_IND_BLOCK: usize = EXT4_NDIR_BLOCKS;
pub const EXT4_DIND_BLOCK: usize = EXT4_IND_BLOCK + 1;
pub const EXT4_TIND_BLOCK: usize = EXT4_DIND_BLOCK + 1;
pub const EXT4_N_BLOCKS: usize = EXT4_TIND_BLOCK + 1;

pub const EXT4_SUPER_BLOCK_OFFSET: usize = 1024;
pub const EXT4_SUPER_BLOCK_SIZE: usize = 1024;

pub const EXT4_BOOT_BLOCK_SIZE: usize = 1024;
pub const EXT4_LABEL_LEN: usize = 16;

pub const EXT4_BAD_INO: u32 = 1; /* Bad blocks inode */
pub const EXT4_ROOT_INO: u32 = 2; /* Root inode */
pub const EXT4_USR_QUOTA_INO: u32 = 3; /* User quota inode */
pub const EXT4_GRP_QUOTA_INO: u32 = 4; /* Group quota inode */
pub const EXT4_BOOT_LOADER_INO: u32 = 5; /* Boot loader inode */
pub const EXT4_UNDEL_DIR_INO: u32 = 6; /* Undelete directory inode */
pub const EXT4_RESIZE_INO: u32 = 7; /* Reserved group descriptors inode */
pub const EXT4_JOURNAL_INO: u32 = 8; /* Journal inode */
pub const EXT4_EXCLUDE_INO: u32 = 9; /* The "exclude" inode, for snapshots */
pub const EXT4_REPLICA_INO: u32 = 10; /* Used by non-upstream feature */

pub const EXT4_SUPER_MAGIC: u16 = 0xEF53;

pub const EXT2_SECRM_FL: u32 = 0x00000001; /* Secure deletion */
pub const EXT2_UNRM_FL: u32 = 0x00000002; /* Undelete */
pub const EXT2_COMPR_FL: u32 = 0x00000004; /* Compress file */
pub const EXT2_SYNC_FL: u32 = 0x00000008; /* Synchronous updates */
pub const EXT2_IMMUTABLE_FL: u32 = 0x00000010; /* Immutable file */
pub const EXT2_APPEND_FL: u32 = 0x00000020; /* writes to file may only append */
pub const EXT2_NODUMP_FL: u32 = 0x00000040; /* do not dump file */
pub const EXT2_NOATIME_FL: u32 = 0x00000080; /* do not update atime */
/* Reserved for compression usage... */
pub const EXT2_DIRTY_FL: u32 = 0x00000100;
pub const EXT2_COMPRBLK_FL: u32 = 0x00000200; /* One or more compressed clusters */
pub const EXT2_NOCOMPR_FL: u32 = 0x00000400; /* Access raw compressed data */
/* nb: was previously EXT2_ECOMPR_FL */
pub const EXT4_ENCRYPT_FL: u32 = 0x00000800; /* encrypted inode */
/* End compression flags --- maybe not all used */
pub const EXT2_BTREE_FL: u32 = 0x00001000; /* btree format dir */
pub const EXT2_INDEX_FL: u32 = 0x00001000; /* hash-indexed directory */
pub const EXT2_IMAGIC_FL: u32 = 0x00002000;
pub const EXT3_JOURNAL_DATA_FL: u32 = 0x00004000; /* file data should be journaled */
pub const EXT2_NOTAIL_FL: u32 = 0x00008000; /* file tail should not be merged */
pub const EXT2_DIRSYNC_FL: u32 = 0x00010000; /* Synchronous directory modifications */
pub const EXT2_TOPDIR_FL: u32 = 0x00020000; /* Top of directory hierarchies*/
pub const EXT4_HUGE_FILE_FL: u32 = 0x00040000; /* Set to each huge file */
pub const EXT4_EXTENTS_FL: u32 = 0x00080000; /* Inode uses extents */
pub const EXT4_VERITY_FL: u32 = 0x00100000; /* Verity protected inode */
pub const EXT4_EA_INODE_FL: u32 = 0x00200000; /* Inode used for large EA */

/*
 * Ext2 directory file types.  Only the low 3 bits are used.  The
 * other bits are reserved for now.
 */
pub const EXT4_FT_UNKNOWN: u8 = 0;
pub const EXT4_FT_REG_FILE: u8 = 1;
pub const EXT4_FT_DIR: u8 = 2;
pub const EXT4_FT_CHRDEV: u8 = 3;
pub const EXT4_FT_BLKDEV: u8 = 4;
pub const EXT4_FT_FIFO: u8 = 5;
pub const EXT4_FT_SOCK: u8 = 6;
pub const EXT4_FT_SYMLINK: u8 = 7;

pub const EXT4_FT_MAX: u8 = 8;

pub const EXT4_BM_FREE: u32 = 0;
pub const EXT4_BM_USED: u32 = 1;

pub const EXT4_MAX_NAME_LEN: usize = 255;

pub const EXT4_EXT_MAGIC: u16 = 0xf30a;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct InodeLargeOsd1 {
    pub l_i_version: u32,
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct InodeLargeOsd2 {
    pub l_i_blocks_hi: u16,
    pub l_i_file_acl_high: u16,
    pub l_i_uid_high: u16,
    pub l_i_gid_high: u16,
    pub l_i_checksum_lo: u16,
    pub l_i_reserved: u16,
}

pub const S_IFMT: u16 = 0o170000; /* These bits determine file type.  */

/* File types.  */
pub const S_IFDIR: u16 = 0o40000; /* Directory.  */
pub const S_IFCHR: u16 = 0o20000; /* Character device.  */
pub const S_IFBLK: u16 = 0o60000; /* Block device.  */
pub const S_IFREG: u16 = 0o100000; /* Regular file.  */
pub const S_IFIFO: u16 = 0o10000; /* FIFO.  */
pub const S_IFLNK: u16 = 0o120000; /* Symbolic link.  */
pub const S_IFSOCK: u16 = 0o140000; /* Socket.  */

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Inode {
    pub i_mode: u16,
    pub i_uid: u16,
    pub i_size: u32,
    pub i_atime: u32,
    pub i_ctime: u32,
    pub i_mtime: u32,
    pub i_dtime: u32,
    pub i_gid: u16,
    pub i_links_count: u16,
    pub i_blocks: u32,
    pub i_flags: u32,
    pub osd1: InodeLargeOsd1,
    pub i_block: [u32; EXT4_N_BLOCKS],
    pub i_generation: u32,
    pub i_file_acl: u32,
    pub i_size_high: u32,
    pub i_faddr: u32,
    pub osd2: InodeLargeOsd2,
    pub i_extra_isize: u16,
    pub i_checksum_hi: u16,
    pub i_ctime_extra: u32,
    pub i_mtime_extra: u32,
    pub i_atime_extra: u32,
    pub i_crtime: u32,
    pub i_crtime_extra: u32,
    pub i_version_hi: u32,
    pub i_projid: u32,
}

impl Debug for Inode {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        let size = ((self.i_size_high as u64) << 32) | self.i_size as u64;
        write!(
            fmt,
            "Inode: {{ i_mode: 0o{:o}, i_uid: {}, i_gid: {}, size: {}, i_links_count: {}, i_blocks: {}, i_block: ",
            self.i_mode, self.i_uid, self.i_gid, size, self.i_links_count, self.i_blocks
        )?;
        if self.i_flags & EXT4_EXTENTS_FL != 0 {
            write!(fmt, "[uses extents]")?;
        } else {
            write!(fmt, "{:?}", self.i_block)?;
        }
        write!(fmt, " }}")?;
        Ok(())
    }
}

pub enum InodeBlock {
    Pointer([u32; EXT4_N_BLOCKS]),
    Extent(ExtentHeader, Extent),
}

impl Inode {
    pub fn is_dir(&self) -> bool {
        self.i_mode & S_IFMT == S_IFDIR
    }

    pub fn is_reg(&self) -> bool {
        self.i_mode & S_IFMT == S_IFREG
    }

    pub fn get_extent_header<'a>(&self) -> Option<&'a ExtentHeader> {
        if self.i_flags & EXT4_EXTENTS_FL == 0 {
            return None;
        }
        unsafe {
            let eh = &self.i_block as *const u32 as *const ExtentHeader;
            Some(eh.as_ref().unwrap())
        }
    }

    pub fn get_extents<'a>(&self) -> Vec<&'a Extent> {
        let mut v = vec![];
        let eh = self.get_extent_header().unwrap();

        for i in 1..=(eh.eh_entries as usize) {
            unsafe {
                let e = (&self.i_block as *const u32).add(3 * i) as *const Extent;
                v.push(e.as_ref().unwrap());
            }
        }

        v
    }
    //     pub fn get_block_pointers(&self) -> InodeBlock {
    //         if self.i_flags & EXT4_EXTENTS_FL != 0 {
    //             unsafe {
    //                 let eh = &self.i_block as *const u32 as *const ExtentHeader;
    //                 let ee = (&self.i_block as *const u32).add(3) as *const Extent;
    //                 InodeBlock::Extent(*eh, *ee)
    //             }
    //         }
    //         else {
    //             InodeBlock::Pointer(self.i_block)
    //         }
    //     }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct SuperBlock {
    pub s_inodes_count: u32,
    pub s_blocks_count: u32,
    pub s_r_blocks_count: u32,
    pub s_free_blocks_count: u32,
    pub s_free_inodes_count: u32,
    pub s_first_data_block: u32,
    pub s_log_block_size: u32,
    pub s_log_cluster_size: u32,
    pub s_blocks_per_group: u32,
    pub s_clusters_per_group: u32,
    pub s_inodes_per_group: u32,
    pub s_mtime: u32,
    pub s_wtime: u32,
    pub s_mnt_count: u16,
    pub s_max_mnt_count: i16,
    pub s_magic: u16,
    pub s_state: u16,
    pub s_errors: u16,
    pub s_minor_rev_level: u16,
    pub s_lastcheck: u32,
    pub s_checkinterval: u32,
    pub s_creator_os: u32,
    pub s_rev_level: u32,
    pub s_def_resuid: u16,
    pub s_def_resgid: u16,
    /*
     * These fields are for EXT2_DYNAMIC_REV superblocks only.
     *
     * Note: the difference between the compatible feature set and
     * the incompatible feature set is that if there is a bit set
     * in the incompatible feature set that the kernel doesn't
     * know about, it should refuse to mount the filesystem.
     *
     * e2fsck's requirements are more strict; if it doesn't know
     * about a feature in either the compatible or incompatible
     * feature set, it must abort and not try to meddle with
     * things it doesn't understand...
     */
    pub s_first_ino: u32,
    pub s_inode_size: u16,
    pub s_block_group_nr: u16,
    pub s_feature_compat: u32,
    pub s_feature_incompat: u32,
    pub s_feature_ro_compat: u32,
    pub s_uuid: [u8; 16],

    pub s_volume_name: [u8; EXT4_LABEL_LEN],
    pub s_last_mounted: [u8; 64],
    pub s_algorithm_usage_bitmap: u32,

    /*
     * Performance hints.  Directory preallocation should only
     * happen if the EXT2_FEATURE_COMPAT_DIR_PREALLOC flag is on.
     */
    pub s_prealloc_blocks: u8,
    pub s_prealloc_dir_blocks: u8,
    pub s_reserved_gdt_blocks: u16,
    /*
     * Journaling support valid if EXT2_FEATURE_COMPAT_HAS_JOURNAL set.
     */
    pub s_journal_uuid: [u8; 16],
    pub s_journal_inum: u32,
    pub s_journal_dev: u32,
    pub s_last_orphan: u32,
    pub s_hash_seed: [u32; 4],
    pub s_def_hash_version: u8,

    pub s_jnl_backup_type: u8,
    pub s_desc_size: u16,
    pub s_default_mount_opts: u32,
    pub s_first_meta_bg: u32,
    pub s_mkfs_time: u32,

    pub s_jnl_blocks: [u32; 17],
    pub s_blocks_count_hi: u32,
    pub s_r_blocks_count_hi: u32,

    pub s_free_blocks_hi: u32,
    pub s_min_extra_isize: u16,
    pub s_want_extra_isize: u16,
    pub s_flags: u32,
    pub s_raid_stride: u16,
    pub s_mmp_update_interval: u16,
    pub s_mmp_block: u64,
    pub s_raid_stripe_width: u32,

    pub s_log_groups_per_flex: u8,
    pub s_checksum_type: u8,
    pub s_encryption_level: u8,
    pub s_reserved_pad: u8,
    pub s_kbytes_written: u64,

    pub s_snapshot_inum: u32,
    pub s_snapshot_id: u32,
    pub s_snapshot_r_blocks_count: u64,
    pub s_snapshot_list: u32,

    pub s_error_count: u32,
    pub s_first_error_time: u32,
    pub s_first_error_ino: u32,
    pub s_first_error_block: u64,
    pub s_first_error_func: [u8; 32],
    pub s_first_error_line: u32,
    pub s_last_error_time: u32,
    pub s_last_error_ino: u32,
    pub s_last_error_line: u32,
    pub s_last_error_block: u64,
    pub s_last_error_func: [u8; 32],

    pub s_mount_opts: [u8; 64],
    pub s_usr_quota_inum: u32,
    pub s_grp_quota_inum: u32,
    pub s_overhead_clusters: u32,
    pub s_backup_bgs: [u32; 2],
    pub s_encrypt_algos: [u8; 4],
    pub s_encrypt_pw_salt: [u8; 16],

    pub s_lpf_ino: u32,
    pub s_prj_quota_inum: u32,
    pub s_checksum_seed: u32,
    pub s_wtime_hi: u8,
    pub s_mtime_hi: u8,
    pub s_mkfs_time_hi: u8,
    pub s_lastcheck_hi: u8,
    pub s_first_error_time_hi: u8,
    pub s_last_error_time_hi: u8,
    pub s_first_error_errcode: u8,
    pub s_last_error_errcode: u8,

    pub s_encoding: u16,
    pub s_encoding_flags: u16,
    pub s_reserved: [u32; 95],
    pub s_checksum: u32,
}

impl Debug for SuperBlock {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        write!(fmt, "SuperBlock: {{ ")?;
        write!(
            fmt,
            "s_inodes_count: {}, s_blocks_count: {} ",
            self.s_inodes_count, self.s_blocks_count
        )?;
        write!(fmt, "s_magic: 0x{:04x}, s_volume_name: \"", self.s_magic)?;
        for i in 0..EXT4_LABEL_LEN {
            let c = self.s_volume_name[i];
            if c == 0 {
                break;
            }
            write!(fmt, "{}", c as char)?;
        }
        write!(fmt, "\", s_log_block_size: {} }}", self.s_log_block_size)?;
        Ok(())
    }
}

impl SuperBlock {
    pub fn block_size(&self) -> u32 {
        1024 << self.s_log_block_size
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct DirEntry {
    pub inode: u32,
    pub rec_len: u16,
    pub name_len: u8,
    pub file_type: u8,
    pub name: [u8; EXT4_MAX_NAME_LEN],
}
impl Debug for DirEntry {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        write!(
            fmt,
            "DirEntry: {{ inode: {}, rec_len: {}, name_len: {}, file_type: {}, name: \"",
            self.inode, self.rec_len, self.name_len, self.file_type
        )?;
        for i in 0..self.name_len {
            let j = i as usize;
            write!(fmt, "{}", self.name[j] as char)?;
        }
        write!(fmt, "\" }}")?;
        Ok(())
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct DirEntryTail {
    pub det_reserved_zero1: u32,
    pub det_rec_len: u16,
    pub det_reserved_name_len: u16,
    pub det_checksum: u32,
}

impl Debug for DirEntryTail {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        write!(
            fmt,
            "DirEntryTail: {{ det_reserved_zero1: {}, det_rec_len: {}, det_reserved_name_len: 0x{:04x}, det_checksum: 0x{:08x} }}",
            self.det_reserved_zero1,
            self.det_rec_len,
            self.det_reserved_name_len,
            self.det_checksum
        )?;
        Ok(())
    }
}

pub const DET_RESERVED_NAME_LEN: u16 = 0xDE00;

#[repr(C)]
#[derive(Copy, Clone)]
pub struct GroupDesc {
    pub bg_block_bitmap: u32,
    pub bg_inode_bitmap: u32,
    pub bg_inode_table: u32,
    pub bg_free_blocks_count: u16,
    pub bg_free_inodes_count: u16,
    pub bg_used_dirs_count: u16,
    pub bg_flags: u16,
    pub bg_exclude_bitmap_lo: u32,
    pub bg_block_bitmap_csum_lo: u16,
    pub bg_inode_bitmap_csum_lo: u16,
    pub bg_checksum: u16,
    pub bg_block_bitmap_hi: u32,
    pub bg_inode_bitmap_hi: u32,
    pub bg_inode_table_hi: u32,
    pub bg_free_blocks_count_hi: u16,
    pub bg_free_inodes_count_hi: u16,
    pub bg_used_dirs_count_hi: u16,
    pub bg_itable_unused_hi: u16,
    pub bg_exclude_bitmap_hi: u32,
    pub bg_block_bitmap_csum_hi: u16,
    pub bg_inode_bitmap_csum_hi: u16,
    pub bg_reserved: u32,
}

impl Debug for GroupDesc {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        write!(
            fmt,
            "GroupDesc: {{ bg_block_bitmap: {}, bg_inode_bitmap: {}, bg_inode_table: {} }}",
            self.bg_block_bitmap, self.bg_inode_bitmap, self.bg_inode_table
        )?;
        Ok(())
    }
}

pub const fn block_offset(sb: &SuperBlock, w: u32) -> usize {
    (EXT4_BOOT_BLOCK_SIZE as u32 + (w - 1) * (1024 << sb.s_log_block_size)) as usize
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct ExtentHeader {
    pub eh_magic: u16,
    pub eh_entries: u16,
    pub eh_max: u16,
    pub eh_depth: u16,
    pub eh_generation: u32,
}

impl Debug for ExtentHeader {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        write!(
            fmt,
            "ExtentHeader: {{ magic: 0x{:04x}, entries: {}, max: {}, depth: {}, generation: {} }}",
            self.eh_magic, self.eh_entries, self.eh_max, self.eh_depth, self.eh_generation
        )?;
        Ok(())
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Extent {
    pub ee_block: u32,    /* first logical block extent covers */
    pub ee_len: u16,      /* number of blocks covered by extent */
    pub ee_start_hi: u16, /* high 16 bits of physical block */
    pub ee_start: u32,    /* low 32 bigs of physical block */
}

impl Debug for Extent {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        let start = ((self.ee_start_hi as u64) << 32) | (self.ee_start as u64);
        write!(
            fmt,
            "Extent: {{ block: {}, len: {}, start: {} }}",
            self.ee_block, self.ee_len, start
        )?;
        Ok(())
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct ExtentIndex {
    pub ei_block: u32, /* index covers logical blocks from 'block' */
    pub ei_leaf: u32,  /* pointer to the physical block of the next *
                        * level. leaf or next index could bet here */
    pub ei_leaf_hi: u16, /* high 16 bits of physical block */
    pub ei_unused: u16,
}

impl Debug for ExtentIndex {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        let leafy = ((self.ei_leaf_hi as u64) << 32) | (self.ei_leaf as u64);
        write!(
            fmt,
            "ExtentIndex: {{ block: {}, leaf: {} }}",
            self.ei_block, leafy
        )?;
        Ok(())
    }
}

use crate::util::read_bytes;
use std::fs::File;
use std::io::BufReader;
pub fn id(reader: &mut BufReader<File>) -> bool {
    if let Ok(sb) = read_bytes::<SuperBlock>(reader, EXT4_BOOT_BLOCK_SIZE) {
        sb.s_magic == EXT4_SUPER_MAGIC
    } else {
        false
    }
}

use crate::vfs::FsDriver;
pub struct Ext4;

impl FsDriver for Ext4 {
    fn name(&self) -> &str {
        "ext4"
    }
}
